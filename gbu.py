#!/usr/bin/python3
import gi			# For the GUI
import os			# To use the file system
import getpass		# To get the username
import time
import subprocess
import socket		# To get the hostname
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio


UIfile = os.path.dirname(os.path.realpath(__file__)) + "/window.ui"
Hostname = socket.gethostname()
DriveLocation = "/media/" + getpass.getuser() + "/BU_Drive"


BackupCommand = ["pkexec", "rsync", "-aHv", "--delete", "--progress", "/etc", "/home", DriveLocation + "/BU_Backups/" + Hostname + "/"]
RestoreCommand = ["pkexec", "rsync", "-aHv", "--delete", "--progress", DriveLocation + "/BU_Backups/" + Hostname + "/home/", "/home/"]


def GetDriveStatus():
	return(os.path.isdir(DriveLocation))
	
def GetRestoreStatus():
	return(os.path.isdir(DriveLocation + "/BU_Backups/" + Hostname))

class TOOL(Gtk.Application):
	
	def __init__(self):
		Gtk.Application.__init__(self, application_id='org.smurphy.gbu', flags=Gio.ApplicationFlags.FLAGS_NONE)
		self.connect("activate", self.OnActivate)
		
	def OnActivate(self, data=None):
		ActiveWindows = self.get_windows()
		if len(ActiveWindows) > 0:
			# Focus window if aleardy running
			self.get_active_window().present()
		else:
			self.CreateMainWindow()
			
			
	def CreateMainWindow(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file(UIfile)
		self.window = self.builder.get_object("window1")
		
		self.window.set_icon_name("gbu")
		
		self.window.show_all()
		self.add_window(self.window)
		
		self.ButtonBackup = self.builder.get_object("ButtonBackup")
		self.ButtonRestore = self.builder.get_object("ButtonRestore")
		self.ButtonRefresh = self.builder.get_object("ButtonRefresh")
		self.ButtonAbout = self.builder.get_object("ButtonAbout")
		self.LabelDriveStatus = self.builder.get_object("LabelDriveStatus")
		self.SpinnerMain = self.builder.get_object("SpinnerMain")
		
		# Connect Buttons to there actions
		self.ButtonBackup.connect("clicked", self.backup)
		self.ButtonRestore.connect("clicked", self.RestoreDialog)
		self.ButtonRefresh.connect("clicked", self.update)
		self.ButtonAbout.connect("clicked", self.about)

		
		# Set label to show if drive is found
		#Note "none" when using the self.update method is only there to
		# prevent a typerror
		self.update("none")
		
	# Backup
	def backup(self, button):
		if self.update("none"):
			self.LabelDriveStatus.set_text("Backing up...")
			self.builder.get_object("SpinnerMain").start()
			time.sleep(1)
			while Gtk.events_pending():
				Gtk.main_iteration_do(True)
			if not os.path.isdir(DriveLocation + "/BU_Backups"):
				os.mkdir(DriveLocation + "/BU_Backups/")
			if not os.path.isdir(DriveLocation + "/BU_Backups/" + Hostname):
				os.mkdir(DriveLocation + "/BU_Backups/" + Hostname)
			ExitCode = subprocess.Popen(BackupCommand).wait()
			if ExitCode == 0:					
				self.SuccessDialog("Backup Complete")
			elif ExitCode != 126:
				self.ErrorDialog("An error accored during the backup \nError Code: " + str(ExitCode))
			self.update("none")
			
				
	# Restore		
	def RestoreDialog(self, button):
		self.window.set_sensitive(0)
		self.builder = Gtk.Builder()
		self.builder.add_from_file(UIfile)
		
		self.DialogConfirmRestore = self.builder.get_object("DialogConfirmRestore")
		self.ButtonCancelRestore = self.builder.get_object("ButtonCancelRestore")
		self.ButtonConfirmRestore = self.builder.get_object("ButtonConfirmRestore")
		
		self.ButtonCancelRestore.connect("clicked", self.CancelRestore)
		self.ButtonConfirmRestore.connect("clicked", self.ConfirmRestore)
		self.DialogConfirmRestore.connect("destroy", self.update)
		
		self.DialogConfirmRestore.show_all()
		self.add_window(self.DialogConfirmRestore)
	
	
	def CancelRestore(self, button):
		self.DialogConfirmRestore.destroy()
		self.update("none")
	
	def ConfirmRestore(self, button):
		self.DialogConfirmRestore.destroy()
		while Gtk.events_pending():
			Gtk.main_iteration_do(True)
		self.restore("none")
		
	def restore(self, button):
		if self.update("none"):
			self.LabelDriveStatus.set_text("Restoring...")
			self.builder.get_object("SpinnerMain").start()
			time.sleep(1)
			while Gtk.events_pending():
				Gtk.main_iteration_do(True)
			ExitCode = subprocess.Popen(RestoreCommand).wait()
			if ExitCode == 0:
				self.SuccessDialog("Restoring complete!\nPlease Restart your computer")
			elif ExitCode != 126:
				self.ErrorDialog("An error accored during restoring \nError Code: " + str(ExitCode))
			self.update("none")
			
	# Updating the UI and drive status	
	def update(self, button):
		
		self.SpinnerMain.stop()
		while Gtk.events_pending():
			Gtk.main_iteration_do(True)
		if GetDriveStatus():
			self.LabelDriveStatus.set_text("Backup Drive is ready!")
			self.ButtonBackup.set_sensitive(1)
			


		else:
			self.LabelDriveStatus.set_text("Backup Drive is not found!")
			self.ButtonBackup.set_sensitive(0)
			self.ButtonRestore.set_sensitive(0)
			return(False)
			
		if GetRestoreStatus():
			self.ButtonRestore.set_sensitive(1)
			self.ButtonRestore.set_tooltip_text("")
			return(True)
			
		else:
			self.ButtonRestore.set_sensitive(0)
			self.ButtonRestore.set_tooltip_text("There is not backups to restore from")
			return(True)
			

	# Errors	
	def ErrorDialog(self, message):
		self.builder = Gtk.Builder()
		self.builder.add_from_file(UIfile)
		
		self.DialogError = self.builder.get_object("DialogError")
		self.LabelError = self.builder.get_object("LabelError")
		self.ButtonCloseError = self.builder.get_object("ButtonCloseError")
		
		self.ButtonCloseError.connect("clicked", self.CloseErrorDialog)
		self.DialogError.connect("destroy", self.update)
		
		self.LabelError.set_text(message)
		self.DialogError.show_all()
		self.add_window(self.DialogError)
		
	def CloseErrorDialog(self, button):
		self.DialogError.destroy()
		
		
	# Success
	def SuccessDialog(self, message):
		self.builder = Gtk.Builder()
		self.builder.add_from_file(UIfile)
		
		self.DialogSuccess = self.builder.get_object("DialogSuccess")
		self.LabelSuccess = self.builder.get_object("LabelSuccess")
		self.ButtonCloseSuccess = self.builder.get_object("ButtonCloseSuccess")
		
		self.ButtonCloseSuccess.connect("clicked", self.CloseSuccessDialog)
		self.DialogSuccess.connect("destroy", self.update)
		
		self.LabelSuccess.set_text(message)
		self.DialogSuccess.show_all()
		self.add_window(self.DialogSuccess)
		
	def CloseSuccessDialog(self, button):
		self.DialogSuccess.destroy()	
			
	# About Window	
	def about(self, button):
		self.builder = Gtk.Builder()
		self.builder.add_from_file(UIfile)
		self.AboutWindow = self.builder.get_object("WindowAbout")
		self.AboutWindow.show_all()
		self.add_window(self.AboutWindow)
		



if __name__ == "__main__":
    app = TOOL()
    app.run(None)
