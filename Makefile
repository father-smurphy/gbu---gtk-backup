PREFIX = /usr
BINPREFIX = $(PREFIX)/bin
ICONPREFIX = $(PREFIX)/share/icons
DESKTOPPREFIX = $(PREFIX)/share/applications
GBUPPREFIX = $(PREFIX)/share/gbu
MANPREFIX = $(PREFIX)/local/share/man/man1

install:
	install -D -m 755 bin/root $(BINPREFIX)/gbu
	install -D -m 644 gbu.png $(ICONPREFIX)/gbu.png
	install -D -m 644 gbu.desktop $(DESKTOPPREFIX)/gbu.desktop
	install -D -m 644 gbu.py $(GBUPPREFIX)/gbu.py
	install -D -m 644 window.ui $(GBUPPREFIX)/window.ui
	install -D -m 644 doc/gbu.1 $(MANPREFIX)/man1/gbu.1
	

uninstall:
	$(RM) $(BINPREFIX)/gbu
	$(RM) $(DESKTOPPREFIX)/gbu.desktop
	$(RM) $(ICONPREFIX)/gbu.png
	$(RM) $(GBUPPREFIX)/*
